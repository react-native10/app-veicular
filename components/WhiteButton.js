import React from 'react';

import { Text, TouchableOpacity, StyleSheet } from 'react-native';

export default props => {
    return (
        <TouchableOpacity onPress={props.onPress} style={css.wrap}>
            <Text style={css.text}>{props.text}</Text>
        </TouchableOpacity>
    )
}

const css = StyleSheet.create({
    wrap: {
      padding: 10,
    },
    text: {
        color: 'white',
    }
  });
