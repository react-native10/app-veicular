import React from 'react';
import { View, Text, StyleSheet, ImageBackground, Button, ActivityIndicator } from 'react-native';
import BGImage from '../assets/background-welcome.png';


export default class LoadingScreen extends React.Component {

  componentDidMount() {
    // Start counting when the page is loaded
    this.timeoutHandle = setTimeout(() => {
      return this.props.navigation.navigate('ChooseCity')
    }, 4000);
  }

  static navigationOptions = {
    header: null,
  };

  render() {
    return (
      <ImageBackground source={BGImage} style={css.bg}>
        <View style={css.view}>

          <ActivityIndicator size="large" color="#fffff" />
          <Text style={css.text}>Carregando cidades...</Text>
          <Text></Text>
          {/* <Button color="#841584" title="Continuar >>" onPress={() => this.props.navigation.navigate('ChooseCity')} /> */}
        </View>
      </ImageBackground>
    )
  }
}


const css = StyleSheet.create({
  bg: {
    width: '100%',
    height: '100%',
    backgroundColor: '#262626',
  },
  text: {
    fontWeight: 'bold',
    color: 'white',
    marginTop: 15
  },
  view: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  row: {
    flexDirection: 'row'
  }
});


