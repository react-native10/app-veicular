import React from "react";

import SignupForm from "./SignupForm";
import Server from "../../constants/Server";
import get from "get-value";

export default class SignupScreen extends React.Component {
  static navigationOptions = {
    header: null
  };

  render() {
    return <SignupForm navigation={this.props.navigation} />;
  }
}
