import React, { Component } from "react";
import {
  StyleSheet,
  TouchableOpacity,
  Text,
  View,
  ImageBackground,
  Button
} from "react-native";
import WhiteButton from "../../components/WhiteButton";
import { SocialIcon } from "react-native-elements";

export default class LoginTypeScreen extends React.Component {
  _findGarage = () => {
    return this.props.navigation.navigate("Loading");
  };

  _login = () => {
    return this.props.navigation.navigate("Login");
  };

  _signup = () => {
    return this.props.navigation.navigate("Signup");
  };

  _entrar = () => {
    return this.props.navigation.navigate("Dashboard");
  };

  render() {
    return (
      <ImageBackground
        source={require("../../assets/background-welcome.png")}
        style={{ width: "100%", height: "100%" }}
      >
        <View style={styles.container}>
          <View style={styles.boxButtons}>
            <View>
              <Text style={styles.simpleText}> Como deseja entrar? </Text>
            </View>

            <TouchableOpacity onPress={this._login}>
              <SocialIcon
                style={styles.socialButton}
                title="Com E-mail e Senha"
                button
                type="sign-in"
                fontStyle={styles.socialButtonTextColor}
                iconStyle={styles.socialButtonTextColor}
              />
            </TouchableOpacity>
            <TouchableOpacity onPress={this._entrar}>
              <SocialIcon
                title="Sign In With Gmail"
                button
                type="google-plus-official"
              />
            </TouchableOpacity>
            <TouchableOpacity onPress={this._entrar}>
              <SocialIcon
                title="Sign In With Facebook"
                button
                type="facebook"
              />
            </TouchableOpacity>
          </View>
        </View>
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    paddingHorizontal: 10
  },
  boxButtons: {
    marginTop: 20
  },
  button: {
    alignItems: "center",
    backgroundColor: "#ffffff",
    padding: 10,
    borderRadius: 30,
    margin: 10,
    height: 50
  },
  socialButton: {
    backgroundColor: "#fff"
  },
  socialButtonTextColor: {
    color: "#000"
  },
  buttonText: {
    color: "#472e99"
  },
  forgetPassword: {
    alignItems: "center",
    padding: 0
  },
  simpleText: {
    color: "#FFFFFF",
    fontSize: 13,
    paddingBottom: 15,
    alignItems: "center"
  }
});
