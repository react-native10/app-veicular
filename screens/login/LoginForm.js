import React from "react";
import {
  View,
  ImageBackground,
  ScrollView,
  StyleSheet,
  Image,
  TextInput,
  Text,
  TouchableOpacity
} from "react-native";

import { Button, Input } from "react-native-elements";

import BGImage from "../../assets/login-bg.png";
// import Logo from '../../assets/white-logo.png';
import WhiteButton from "../../components/WhiteButton";

import { Formik } from "formik";
import * as yup from "yup";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";

const FAKE_DATA = {
  email: "devmaker.leonidas@gmail.com",
  password: "123456"
};

export default class LoginForm extends React.Component {
  static navigationOptions = {
    header: null
  };

  _entrar = () => {
    return this.props.navigation.navigate("Dashboard");
  };

  render() {
    return (
      <ImageBackground source={BGImage} style={css.bg}>
        <Formik initialValues={FAKE_DATA} onSubmit={this._submit}>
          {form => (
            <KeyboardAwareScrollView contentContainerStyle={css.scroll}>
              {/* <Image source={Logo} style={css.logo} /> */}
              <Input
                inputStyle={css.input}
                placeholder="Seu E-mail"
                onChangeText={form.handleChange("email")}
                value={form.values.email}
              />
              <Input
                inputStyle={css.input}
                placeholder="Sua Senha"
                onChangeText={form.handleChange("password")}
                value={form.values.password}
              />
              <TouchableOpacity style={styles.button} onPress={this._entrar}>
                <Text style={styles.buttonText}> Acessar minha conta </Text>
              </TouchableOpacity>
              <WhiteButton text="Esqueceu sua senha?" />
            </KeyboardAwareScrollView>
          )}
        </Formik>
      </ImageBackground>
    );
  }

  _submit = (values, actions) => {
    this.props.onSubmit(values);
  };
}

const css = StyleSheet.create({
  bg: {
    width: "100%",
    height: "100%",
    backgroundColor: "#262626"
  },
  scroll: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center"
  },
  input: {
    color: "white"
  },
  logo: {
    width: "45%",
    height: 60,
    resizeMode: "contain"
  },
  row: {
    flexDirection: "row"
  }
});

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    paddingHorizontal: 10
  },
  boxButtons: {
    marginTop: 20
  },
  button: {
    alignItems: "center",
    backgroundColor: "#ffffff",
    padding: 10,
    borderRadius: 30,
    margin: 10
  },
  buttonText: {
    color: "#472e99"
  },
  forgetPassword: {
    alignItems: "center",
    padding: 0
  },
  simpleText: {
    color: "#FFFFFF",
    fontSize: 13,
    paddingBottom: 15,
    alignItems: "center"
  }
});
