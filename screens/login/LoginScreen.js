import React from 'react';

import LoginForm from './LoginForm';
import Server from '../../constants/Server';
import get from 'get-value';



export default class LoginScreen extends React.Component {
  static navigationOptions = {
    header: null,
  };

  render() {
    return (
        <LoginForm navigation={this.props.navigation}/>
    );
  }


}

