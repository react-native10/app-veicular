import React, { Component } from "react";
import {
  StyleSheet,
  TouchableOpacity,
  Text,
  View,
  ImageBackground,
  Button
} from "react-native";
import WhiteButton from "../components/WhiteButton";

export default class HomeScreen extends React.Component {
  _findGarage = () => {
    return this.props.navigation.navigate("Loading");
  };

  _login = () => {
    return this.props.navigation.navigate("Login");
  };

  _signup = () => {
    return this.props.navigation.navigate("Signup");
  };

  _loginType = () => {
    return this.props.navigation.navigate("LoginType");
  };

  render() {
    return (
      <ImageBackground
        source={require("../assets/background-welcome.png")}
        style={{ width: "100%", height: "100%" }}
      >
        <View style={styles.container}>
          <View style={styles.boxButtons}>
            <View>
              <Text style={styles.simpleText}> O que você deseja fazer? </Text>
            </View>

            <TouchableOpacity style={styles.button} onPress={this._findGarage}>
              <Text style={styles.buttonText}> Buscar Vagas </Text>
            </TouchableOpacity>
            <View></View>
            <TouchableOpacity style={styles.button} onPress={this._loginType}>
              <Text style={styles.buttonText}> Acessar minha conta </Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.button} onPress={this._signup}>
              <Text style={styles.buttonText}> Criar nova conta </Text>
            </TouchableOpacity>
          </View>
        </View>
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    paddingHorizontal: 10
  },
  boxButtons: {
    marginTop: 20
  },
  button: {
    alignItems: "center",
    backgroundColor: "#ffffff",
    padding: 10,
    borderRadius: 30,
    margin: 10
  },
  buttonText: {
    color: "#472e99"
  },
  forgetPassword: {
    alignItems: "center",
    padding: 0
  },
  simpleText: {
    color: "#FFFFFF",
    fontSize: 13,
    paddingBottom: 15,
    alignItems: "center"
  }
});
