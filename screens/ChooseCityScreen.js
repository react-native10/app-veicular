import React, { Component } from "react";
import { ImageBackground, Text, StyleSheet, View, Button, Picker, Alert } from "react-native";
import BGImage from '../assets/easy-garage-bg.png';

export default class ChooseCityScreen extends Component {

  constructor() {
    super();
    this.state = {
      PickerSelectedVal: '',
    }
    //console.log('cidade state: ' + this.state.PickerSelectedVal);
  }

  getSelectedPickerValue = () => {
    //Alert.alert("Você selecionou " + this.state.PickerSelectedVal);
    return this.props.navigation.navigate('ChoosePlace')
  }

  getSelectedPickerValueChange = (itemValue, itemIndex) => {
    this.setState({ PickerSelectedVal: itemValue });
    return this.props.navigation.navigate('ChoosePlace')
  }

  render() {
    return (
      <ImageBackground source={BGImage} style={css.bg}>
        <View style={css.view}>

          <Text style={css.text}>Selecione sua cidade</Text>
          <Picker style={css.piker}
            selectedValue={this.state.PickerSelectedVal}
            onValueChange={(itemValue, itemIndex) => this.getSelectedPickerValueChange(itemValue, itemIndex)} >
            <Picker.Item label="Cidades Disponíveis: " value="0" />
            <Picker.Item label="Curitiba - PR" value="CWB" />
            <Picker.Item label="São Paulo - SP" value="SP" />
            <Picker.Item label="Rio de Janeiro - RJ" value="RJ" />
            <Picker.Item label="Belém - PA" value="BEL" />
          </Picker>

          {/* <Button title="Continuar" onPress={this.getSelectedPickerValue} /> */}
        </View>
      </ImageBackground>

    );
  }

}

const css = StyleSheet.create({
  bg: {
    width: '100%',
    height: '100%',
    backgroundColor: '#262626',
  },
  text: {
    fontWeight: 'bold',
    color: 'white',
    marginBottom: 15
  },
  textPiker: {
    fontWeight: 'bold',
    color: 'white',
    marginBottom: 15,
    display: 'none'
  },
  view: {
    flex: 1,
    justifyContent: "center",
    margin: 30
  },
  piker: {
    backgroundColor: '#fff',
    marginBottom: 30
  },
  piker2: {
    backgroundColor: '#fff',
    marginBottom: 30,
    display: 'none'
  },
  btnNext: {
    display: 'none',
  },
  row: {
    flexDirection: 'row'
  }
});