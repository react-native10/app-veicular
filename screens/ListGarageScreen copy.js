import React, { Component } from "react";
import {
  ImageBackground,
  Text,
  StyleSheet,
  View,
  Button,
  Picker,
  Alert,
  FlatList,
  Dimensions
} from "react-native";
import BGImage from "../assets/bg-default.png";
import { ListItem } from "react-native-elements";

const list = [
  {
    name: "Condomínio Arvoredo",
    avatar_url:
      "https://www.curitibanaimoveis.com.br/wp-content/themes/Avenue/timthumb.php?src=http://www.curitibanaimoveis.com.br/wp-content/uploads/2017/05/18157710_301179610316404_984966819184176557_n.jpg&h=300&w=650&zc=1",
    subtitle: "0,5 Km - Xaxim"
  },
  {
    name: "Residencial Vila Camões",
    avatar_url:
      "https://gcn.net.br/dir-arquivo-imagem/2013/12/%7B20131228213214%7D_g1_2.jpg",
    subtitle: "2,6 Km - Xaxim"
  },
  {
    name: "Condomínio Sunrine",
    avatar_url:
      "https://www.paysage.com.br/wp-content/uploads/sobrados-condominio-fechado-santa-candida-curitiba-paysage-sunrise.jpg",
    subtitle: "3,2 Km - Boqueirão"
  },
  {
    name: "Condomínio Royale",
    avatar_url:
      "https://www.paysage.com.br/wp-content/uploads/condominio-fechado-campo-comprido-curitiba-paysage-royale.jpg",
    subtitle: "5,2 Km - Jardim das Américas"
  },
  {
    name: "La Serena Plaza",
    avatar_url:
      "https://www.gazetadopovo.com.br/haus/wp-content/uploads/2018/03/ayoshii-la-serena-praca-espanha-curitiba-novo-edificio-empreendimento-exclusivo-haus-gazeta-do-povo-14-914x564.jpg",
    subtitle: "7,3 Km - Batel"
  },
  {
    name: "Residencial Green Vallye",
    avatar_url:
      "https://baudosantigos.com.br/wp-content/uploads/2017/11/1611-entret-nost-mat-rebou%C3%A7as-lancamento-silva-jardins-fachada-8.jpg",
    subtitle: "11 Km - Santa Felicidade"
  },
  {
    name: "Condomínio Columbia",
    avatar_url:
      "https://www.residencialcostaverde.com.br/wp-content/uploads/2017/11/rcv2018b3.jpeg",
    subtitle: "14 Km - Campo Magro"
  },
  {
    name: "Condomínio Arvoredo",
    avatar_url:
      "https://www.curitibanaimoveis.com.br/wp-content/themes/Avenue/timthumb.php?src=http://www.curitibanaimoveis.com.br/wp-content/uploads/2017/05/18157710_301179610316404_984966819184176557_n.jpg&h=300&w=650&zc=1",
    subtitle: "0,5 Km - Xaxim"
  },
  {
    name: "Residencial Vila Camões",
    avatar_url:
      "https://gcn.net.br/dir-arquivo-imagem/2013/12/%7B20131228213214%7D_g1_2.jpg",
    subtitle: "2,6 Km - Xaxim"
  }
];

export default class ListGarageScreen extends Component {
  constructor() {
    super();
    this.state = {
      PickerSelectedVal: ""
    };
    console.log("cidade state: " + this.state.PickerSelectedVal);
  }

  renderItem = ({ item }) => (
    <ListItem
      title={item.name}
      subtitle={item.subtitle}
      leftAvatar={{ source: { uri: item.avatar_url } }}
      bottomDivider
      chevron
    />
  );

  keyExtractor = (item, index) => index.toString();

  getSelectedPickerValue = () => {
    //Alert.alert("Você selecionou " + this.state.PickerSelectedVal);
    return this.props.navigation.navigate("Dashboard");
  };

  randValue = () => {
    var min = 1;
    var max = 100;
    var rand = min + Math.random() * (max - min);

    return this.float2int(rand);
  };

  float2int(value) {
    return value | 0;
  }
  getSelectedPickerValueChange = (itemValue, itemIndex) => {
    this.setState({ PickerSelectedVal: itemValue });
    //Alert.alert("Você selecionou " + itemValue);
  };

  render() {
    let width = Dimensions.get("screen").width / 5 - 8;

    return (
      <View style={{ flex: 1.0, marginTop: 25 }}>
        <FlatList
          columnWrapperStyle={{ justifyContent: "space-between" }}
          data={[
            { key: "1" },
            { key: "2" },
            { key: "3" },
            { key: "4" },
            { key: "5" },
            { key: "6" },
            { key: "7" },
            { key: "8" },
            { key: "9" },
            { key: "10" },
            { key: "11" },
            { key: "12" },
            { key: "13" },
            { key: "14" },
            { key: "15" },
            { key: "16" },
            { key: "17" },
            { key: "18" },
            { key: "19" },
            { key: "20" },
            { key: "21" },
            { key: "22" },
            { key: "23" },
            { key: "24" },
            { key: "25" },
            { key: "26" },
            { key: "27" },
            { key: "28" },
            { key: "29" },
            { key: "30" },
            { key: "31" },
            { key: "32" },
            { key: "33" },
            { key: "34" },
            { key: "35" },
            { key: "36" },
            { key: "37" },
            { key: "38" },
            { key: "39" },
            { key: "40" },
            { key: "41" },
            { key: "42" },
            { key: "43" },
            { key: "44" },
            { key: "45" },
            { key: "46" },
            { key: "47" },
            { key: "48" },
            { key: "49" },
            { key: "50" },
            { key: "51" },
            { key: "52" },
            { key: "53" },
            { key: "54" },
            { key: "55" },
            { key: "56" },
            { key: "57" },
            { key: "58" },
            { key: "59" },
            { key: "60" }
          ]}
          keyExtractor={item => item.itemId}
          horizontal={false}
          numColumns={5}
          renderItem={({ item, index }) => (
            <View
              style={{
                backgroundColor: "#472e99",
                width: width,
                height: width,
                margin: 4,
                justifyContent: "center"
              }}
            >
              <Text style={css.text} key={item}>
                {index}
              </Text>
            </View>
          )}
        />
      </View>
    );
  }
}

const css = StyleSheet.create({
  columnWrapper: {
    justifyContent: "space-around"
  },
  text: {
    fontWeight: "bold",
    color: "white",
    textAlign: "center"
  },
  bg: {
    width: "100%",
    height: "100%"
  },
  row: {
    flexDirection: "row"
  }
});
