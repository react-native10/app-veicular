import React, { Component } from "react";
import {
  ImageBackground,
  Text,
  StyleSheet,
  View,
  Button,
  Picker,
  Alert,
  FlatList
} from "react-native";
import BGImage from "../assets/bg-default.png";
import { ListItem } from "react-native-elements";

const list = [
  {
    name: "Condomínio Arvoredo",
    avatar_url:
      "https://www.curitibanaimoveis.com.br/wp-content/themes/Avenue/timthumb.php?src=http://www.curitibanaimoveis.com.br/wp-content/uploads/2017/05/18157710_301179610316404_984966819184176557_n.jpg&h=300&w=650&zc=1",
    subtitle: "0,5 Km - Xaxim"
  },
  {
    name: "Residencial Vila Camões",
    avatar_url:
      "https://gcn.net.br/dir-arquivo-imagem/2013/12/%7B20131228213214%7D_g1_2.jpg",
    subtitle: "2,6 Km - Xaxim"
  },
  {
    name: "Condomínio Sunrine",
    avatar_url:
      "https://www.paysage.com.br/wp-content/uploads/sobrados-condominio-fechado-santa-candida-curitiba-paysage-sunrise.jpg",
    subtitle: "3,2 Km - Boqueirão"
  },
  {
    name: "Condomínio Royale",
    avatar_url:
      "https://www.paysage.com.br/wp-content/uploads/condominio-fechado-campo-comprido-curitiba-paysage-royale.jpg",
    subtitle: "5,2 Km - Jardim das Américas"
  },
  {
    name: "La Serena Plaza",
    avatar_url:
      "https://www.gazetadopovo.com.br/haus/wp-content/uploads/2018/03/ayoshii-la-serena-praca-espanha-curitiba-novo-edificio-empreendimento-exclusivo-haus-gazeta-do-povo-14-914x564.jpg",
    subtitle: "7,3 Km - Batel"
  },
  {
    name: "Residencial Green Vallye",
    avatar_url:
      "https://baudosantigos.com.br/wp-content/uploads/2017/11/1611-entret-nost-mat-rebou%C3%A7as-lancamento-silva-jardins-fachada-8.jpg",
    subtitle: "11 Km - Santa Felicidade"
  },
  {
    name: "Condomínio Columbia",
    avatar_url:
      "https://www.residencialcostaverde.com.br/wp-content/uploads/2017/11/rcv2018b3.jpeg",
    subtitle: "14 Km - Campo Magro"
  }
];

export default class ChoosePlaceScreen extends Component {
  constructor() {
    super();
    this.state = {
      PickerSelectedVal: ""
    };
    console.log("cidade state: " + this.state.PickerSelectedVal);
  }

  _listGarage = () => {
    return this.props.navigation.navigate("ListGarage");
  };

  renderItem = ({ item }) => (
    <ListItem
      title={item.name}
      subtitle={item.subtitle}
      leftAvatar={{ source: { uri: item.avatar_url } }}
      bottomDivider
      chevron
      onPress={this._listGarage}
      badge={{
        value: " " + this.randValue() + " Vagas ",
        textStyle: { color: "white" },
        containerStyle: { marginTop: -20 }
      }}
    />
  );

  keyExtractor = (item, index) => index.toString();

  getSelectedPickerValue = () => {
    //Alert.alert("Você selecionou " + this.state.PickerSelectedVal);
    return this.props.navigation.navigate("Dashboard");
  };

  randValue = () => {
    var min = 1;
    var max = 100;
    var rand = min + Math.random() * (max - min);

    return this.float2int(rand);
  };

  float2int(value) {
    return value | 0;
  }
  getSelectedPickerValueChange = (itemValue, itemIndex) => {
    this.setState({ PickerSelectedVal: itemValue });
    //Alert.alert("Você selecionou " + itemValue);
  };

  render() {
    return (
      <ImageBackground source={BGImage} style={css.bg}>
        <View style={css.view}>
          <View style={css.view3}>
            <Text style={[css.text]}> Selecione o local em Curitiba - PR:</Text>
          </View>
          <FlatList
            keyExtractor={this.keyExtractor}
            data={list}
            renderItem={this.renderItem}
          />
        </View>
      </ImageBackground>
    );
  }
}

const css = StyleSheet.create({
  bg: {
    width: "100%",
    height: "100%"
  },
  text: {
    fontWeight: "bold",
    color: "white"
  },
  textPiker: {
    fontWeight: "bold",
    color: "white",
    marginBottom: 15,
    display: "none"
  },
  view: {
    flex: 1,
    justifyContent: "center",
    marginTop: 130
  },
  view3: {
    marginTop: 20,
    marginBottom: 20
  },
  piker: {
    backgroundColor: "#fff",
    marginBottom: 30
  },
  btnNext: {
    display: "none"
  },
  row: {
    flexDirection: "row"
  }
});
