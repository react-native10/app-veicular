import React, { Component } from "react";
import {
  View,
  StyleSheet,
  Image,
  ImageBackground,
  Text,
  TouchableOpacity
} from "react-native";

export default class JustifyContentBasics extends Component {
  _logout = () => {
    return this.props.navigation.navigate("Logout");
  };

  _findGarage = () => {
    return this.props.navigation.navigate("Loading");
  };

  _press = () => {};

  render() {
    return (
      <ImageBackground
        source={require("../assets/background.png")}
        style={{ width: "100%", height: "100%" }}
      >
        <View style={styles.container}>
          <View style={styles.boxTopo}>
            <Image
              style={styles.logoTopo}
              source={require("../assets/logo.png")}
            />
          </View>
          <View style={styles.row}>
            <View style={styles.column}>
              <TouchableOpacity style={styles.Touch} onPress={() => this._findGarage()}>
                <Image
                  style={styles.icoStyle}
                  source={require("../assets/icons/find.png")}
                />
                <Text style={styles.TextIco}>Buscar Vagas</Text>
              </TouchableOpacity>
            </View>
            <View style={styles.column}>
              <TouchableOpacity style={styles.Touch} onPress={() => this._press()}>
                <Image
                  style={styles.icoStyle}
                  source={require("../assets/icons/my-garages.png")}
                />
                <Text style={styles.TextIco}>Minhas Vagas</Text>
              </TouchableOpacity>
            </View>
            <View style={styles.column}>
              <TouchableOpacity style={styles.Touch} onPress={() => this._press()}>
                <Image
                  style={styles.icoStyle}
                  source={require("../assets/icons/my-reservations.png")}
                />
              <Text style={styles.TextIco}>Minhas Reservas</Text>
              </TouchableOpacity>

            </View>
          </View>
          <View style={styles.row}>
            <View style={styles.column}>
              <TouchableOpacity style={styles.Touch} onPress={() => this._press()}>
                <Image
                  style={styles.icoStyle}
                  source={require("../assets/icons/add-credit.png")}
                />
                <Text style={styles.TextIco}>Adicionar Créditos</Text>
              </TouchableOpacity>
            </View>
            <View style={styles.column}>
              <TouchableOpacity style={styles.Touch} onPress={() => this._press()}>
                <Image
                  style={styles.icoStyle}
                  source={require("../assets/icons/profile.png")}
                />
                <Text style={styles.TextIco}>Meus Dados</Text>
              </TouchableOpacity>
            </View>
            <View style={styles.column}>
              <TouchableOpacity style={styles.Touch} onPress={() => this._press()}>
                <Image
                  style={styles.icoStyle}
                  source={require("../assets/icons/information.png")}
                />
                <Text style={styles.TextIco}>Locais</Text>
              </TouchableOpacity>
            </View>
          </View>
          <View style={styles.row}>
            <View style={styles.column}>
              <TouchableOpacity style={styles.Touch} onPress={() => this._press()}>
                <Image
                  style={styles.icoStyle}
                  source={require("../assets/icons/notifications.png")}
                />
                <Text style={styles.TextIco}>Notificações</Text>
              </TouchableOpacity>
            </View>
            <View style={styles.column}>
              <TouchableOpacity onPress={() => this._press()}>
                <Image
                  style={styles.icoStyle}
                  source={require("../assets/icons/24-hours.png")}
                />
                <Text style={styles.TextIco}>Suporte</Text>
              </TouchableOpacity>
            </View>
            <View style={styles.column}>
              <TouchableOpacity  onPress={() => this._logout()}>
                <Image
                  style={styles.icoStyle}
                  source={require("../assets/icons/logout.png")}
                />
                <Text style={styles.TextIco}>Sair</Text>
              </TouchableOpacity>
            </View>
          </View>
          <View style={styles.boxRodape} />
        </View>
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "space-around",
    alignItems: "center"
    //backgroundColor: '#472e99',
  },
  row: {
    flexDirection: "row",
    marginLeft: 10,
    marginRight: 10
    //backgroundColor: "white"
  },
  Touch: {
    marginLeft: 10,
  },
  column: {
    margin: 10,
    width: 90,
    height: 90,
    backgroundColor: "white",
    borderRadius: 30,
    alignContent: "center",
    alignItems: "center"
  },
  boxTopo: {
    marginTop: 35,
    height: 50,
    resizeMode: "contain"
    //backgroundColor: "white"
  },
  logoTopo: {
    height: 50
    //backgroundColor: "red"
  },
  icoStyle: {
    marginTop: 10,
    height: 70,
    width: 70,
    resizeMode: "contain"
    //backgroundColor: "red"
  },
  TextIco: {
    paddingTop: 10,
    color: "#fff",
    fontSize: 13,
    alignContent: "center",
    textAlign: "center"
  },
  boxRodape: {
    marginTop: 0,
    width: 300,
    height: 20
    //backgroundColor: "white"
  }
});
