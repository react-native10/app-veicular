import React, { Component } from "react";
import {
  ImageBackground,
  Text,
  StyleSheet,
  View,
  Button,
  Picker,
  Alert,
  FlatList,
  Dimensions,
  TouchableOpacity,
  Image
} from "react-native";
import BGImage from "../assets/bg-default.png";
import ImageTopo from "../assets/condominio.jpg";

import { ListItem } from "react-native-elements";

export default class ListGarageScreen extends Component {
  constructor() {
    super();
    this.state = {
      PickerSelectedVal: ""
    };
    console.log("cidade state: " + this.state.PickerSelectedVal);
  }

  _listGarage = () => {
    return this.props.navigation.navigate("ListGarage");
  };

  renderItem = ({ item }) => (
    <ListItem
      title={item.name}
      subtitle={item.subtitle}
      leftAvatar={{ source: { uri: item.avatar_url } }}
      bottomDivider
      chevron
      onPress={this._listGarage}
      badge={{
        value: " " + this.randValue() + " Vagas ",
        textStyle: { color: "white" },
        containerStyle: { marginTop: -20 }
      }}
    />
  );

  keyExtractor = (item, index) => index.toString();

  getSelectedPickerValue = () => {
    //Alert.alert("Você selecionou " + this.state.PickerSelectedVal);
    return this.props.navigation.navigate("Dashboard");
  };

  randValue = () => {
    var min = 1;
    var max = 100;
    var rand = min + Math.random() * (max - min);

    return this.float2int(rand);
  };

  float2int(value) {
    return value | 0;
  }
  getSelectedPickerValueChange = (itemValue, itemIndex) => {
    this.setState({ PickerSelectedVal: itemValue });
    //Alert.alert("Você selecionou " + itemValue);
  };

  render() {
    return (
      <ImageBackground source={BGImage} style={css.bg}>
        <View>
          <Image source={ImageTopo} style={css.ImageTopo}></Image>
        </View>
        <View style={css.view}>
          <View style={css.view3}>
            <Text style={css.titleText}>Condomínio Arvoredo - Curitiba PR</Text>
            <Text style={css.textTopo}> Selecione uma vaga disponível:</Text>
          </View>

          <View style={{ flex: 1.0, marginTop: 5, backgroundColor: "white" }}>
            <FlatList
              columnWrapperStyle={{ justifyContent: "space-between" }}
              data={[
                { key: "1" },
                { key: "2" },
                { key: "3" },
                { key: "4" },
                { key: "5" },
                { key: "6" },
                { key: "7" },
                { key: "8" },
                { key: "9" },
                { key: "10" },
                { key: "11" },
                { key: "12" },
                { key: "13" },
                { key: "14" },
                { key: "15" },
                { key: "16" },
                { key: "17" },
                { key: "18" },
                { key: "19" },
                { key: "20" },
                { key: "21" },
                { key: "22" },
                { key: "23" },
                { key: "24" },
                { key: "25" },
                { key: "26" },
                { key: "27" },
                { key: "28" },
                { key: "29" },
                { key: "30" },
                { key: "31" },
                { key: "32" },
                { key: "33" },
                { key: "34" },
                { key: "35" },
                { key: "36" },
                { key: "37" },
                { key: "38" },
                { key: "39" },
                { key: "40" },
                { key: "41" },
                { key: "42" },
                { key: "43" },
                { key: "44" },
                { key: "45" },
                { key: "46" },
                { key: "47" },
                { key: "48" },
                { key: "49" },
                { key: "50" },
                { key: "51" },
                { key: "52" },
                { key: "53" },
                { key: "54" },
                { key: "55" },
                { key: "56" },
                { key: "57" },
                { key: "58" },
                { key: "59" },
                { key: "60" }
              ]}
              keyExtractor={item => item.key}
              horizontal={false}
              numColumns={5}
              renderItem={({ item, index }) => (
                <View
                  style={{
                    backgroundColor: "#fff",
                    width: width,
                    height: width,
                    margin: 4,
                    justifyContent: "center"
                  }}
                >
                  <TouchableOpacity>
                    <View
                      style={
                        item.key == 1 || item.key == 8 || item.key == 19
                          ? css.garageRed
                          : css.garageGreen
                      }
                    >
                      <Text style={css.text} key={item.key}>
                        {item.key}
                      </Text>
                    </View>
                  </TouchableOpacity>
                </View>
              )}
            />
          </View>
        </View>
      </ImageBackground>
    );
  }
}
let width = Dimensions.get("screen").width / 5 - 8;

const css = StyleSheet.create({
  bg: {
    width: "100%",
    height: "100%"
  },
  ImageTopo: {
    width: "100%",
    height: 200
  },
  titleText: {
    fontSize: 20,
    fontWeight: "bold",
    color: "white",
    paddingBottom: 4,
    paddingLeft: 5
  },
  textTopo: {
    fontSize: 12,
    fontWeight: "bold",
    color: "white",
    textAlign: "left",
    paddingLeft: 5
  },
  view: {
    flex: 1,
    justifyContent: "center",
    backgroundColor: "#472e99"
  },
  columnWrapper: {
    justifyContent: "space-around"
  },
  garageGreen: {
    borderBottomColor: "#53b35a",
    borderBottomWidth: 2,
    borderTopColor: "#53b35a",
    borderTopWidth: 2,
    borderLeftColor: "#53b35a",
    borderLeftWidth: 2,
    width: width - 10,
    height: width - 20,
    justifyContent: "center",
    marginLeft: 5
  },
  garageRed: {
    borderBottomColor: "red",
    borderBottomWidth: 2,
    borderTopColor: "red",
    borderTopWidth: 2,
    borderLeftColor: "red",
    borderLeftWidth: 2,
    width: width - 10,
    height: width - 20,
    justifyContent: "center",
    marginLeft: 5
  },

  text: {
    fontWeight: "bold",
    color: "black",
    textAlign: "center"
  },
  textPiker: {
    fontWeight: "bold",
    color: "white",
    marginBottom: 15,
    display: "none"
  },
  view3: {
    marginTop: 10,
    marginBottom: 10
  },
  piker: {
    backgroundColor: "#fff",
    marginBottom: 30
  },
  btnNext: {
    display: "none"
  },
  row: {
    flexDirection: "row"
  }
});
