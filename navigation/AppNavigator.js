import React from "react";

import { createSwitchNavigator, createAppContainer } from "react-navigation";

import LoadingScreen from "../screens/LoadingScreen";
import LoginScreen from "../screens/login/LoginScreen";
import ChooseCityScreen from "../screens/ChooseCityScreen";
import ChoosePlaceScreen from "../screens/ChoosePlaceScreen";
import DashboardScreen from "../screens/DashboardScreen";
import HomeScreen from "../screens/HomeScreen";
import LogoutScreen from "../screens/LogoutScreen";
import SignupScreen from "../screens/signup/SignupScreen";
import ListGarageScreen from "../screens/ListGarageScreen";
import LoginTypeScreen from "../screens/login/LoginTypeScreen";

export default createAppContainer(
  createSwitchNavigator(
    {
      Login: LoginScreen,
      Loading: LoadingScreen,
      ChooseCity: ChooseCityScreen,
      ChoosePlace: ChoosePlaceScreen,
      Dashboard: DashboardScreen,
      Home: HomeScreen,
      Logout: LogoutScreen,
      Signup: SignupScreen,
      ListGarage: ListGarageScreen,
      LoginType: LoginTypeScreen
    },
    {
      initialRouteName: "Home"
    }
  )
);
